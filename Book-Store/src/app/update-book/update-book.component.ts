import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { BookService } from '../book.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';

@Component({
  selector: 'app-update-book',
  templateUrl: './update-book.component.html',
  styleUrls: ['./update-book.component.css']
})
export class UpdateBookComponent implements OnInit {
  public i;
  public books: User[] = this.bookService.getBooks();


  constructor(private bookService: BookService,
     private route: ActivatedRoute,
     private router: Router) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.i = parseInt(params.get('Slno'), 10);
    });

  }
  saveChanges() {
    this.bookService.saveBook(this.books);
    this.router.navigate(['/listbooks']);
  }

}
