import { Injectable } from '@angular/core';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor() { }
  public books: User[] = [
    {
      Slno: 1,
      bookName: 'one night at the call centre',
      authorName: 'Chetan Bhagat',
      // tslint:disable-next-line:max-line-length
      description: 'One Night @ the Call Center is a novel written by Chetan Bhagat and first published in 2005.The novel revolves around a group of six call center employees working at the Connexions call center in Gurgaon, Haryana, India. It takes place during one night, during which all of the leading characters confront some aspect of themselves or their lives they would like to change. The story uses a literal deus ex machina, when the characters receive a phone call from God.'
    }
  ];

  getBooks() {
    return this.books;
  }

  addingbook(data) {
    this.books.push(data);
  }
  deleteBook(i) {
    this.books.splice(i, 1);
  }
  saveBook(books) {
    this.books = books;
  }
}
